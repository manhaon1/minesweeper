from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import QFont
from functools import partial
import random
import time 

class Bomb(QMessageBox): 
    def __init__(self, parent, text):
        super().__init__(parent)
        self.setText(text)
        self.setWindowTitle('BOMB!')
        self.button_again = self.addButton('Play again', QMessageBox.YesRole)
        self.button_exit = self.addButton('Exit', QMessageBox.NoRole)
        
class Win(QMessageBox):
    def __init__(self,parent,text):
        super().__init__(parent)
        self.setWindowTitle('You won!')
        self.setText(text)
        self.button_again = self.addButton('Play again', QMessageBox.YesRole)
        self.button_exit = self.addButton('Exit', QMessageBox.NoRole)
        
class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(600,200,1,1)
        self.setWindowTitle('Minesweeper')
        self.resize(300,300)
      
        self.gameField()  
        self.createButtons()
        
        self.flags_num = QLabel()
        self.flags_num.setText('Remaining flags: {}'.format(len(self.bombs_num)))
        self.flags_num.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.flags_num.setFont(QFont('Verdana', 15))
        
        self.clock = QLabel()
        self.clock.setText("Time: 000")
        self.clock.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.clock.setFont(QFont('Verdana', 25))
        
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_timer)
        
        self.opened = [] #seznam otevrenych policek
        
        self.new_game = False #promenna pro novou hru
        
        layout = QGridLayout()
        layout.addWidget(self.GroupBox)
        layout.addWidget(self.clock)
        layout.addWidget(self.flags_num)
        self.setLayout(layout)
    
    def gameField(self): #nahodne rozestavi miny a priradi polickum cisla 
        self.initField=[[0 for i in range(15)]for j in range(15)] #2d pole 15*15
        for x in range (15):
            for y in range (15):
                bomb=random.randint(0,7)
                if bomb==7: #pravdepodobnost vyskytu min
                    self.initField[y][x]='💣'
                    if y-1>=0 and self.initField[y-1][x]!='💣': #vlevo
                        self.initField[y-1][x]+=1
                    if y+1<=14 and self.initField[y+1][x]!='💣': #vpravo
                       self.initField[y+1][x]+=1
                    if x-1>=0 and self.initField[y][x-1]!='💣': #nahore
                        self.initField[y][x-1]+=1
                    if x+1<=14 and self.initField[y][x+1]!='💣': #dole
                        self.initField[y][x+1]+=1
                    if y-1>=0 and x-1>=0 and self.initField[y-1][x-1]!='💣': #vlevo nahore
                        self.initField[y-1][x-1]+=1
                    if y-1>=0 and x+1<=14 and self.initField[y-1][x+1]!='💣': #vlevo dole
                        self.initField[y-1][x+1]+=1
                    if y+1<=14 and x-1>=0 and self.initField[y+1][x-1]!='💣': #vpravo nahore
                        self.initField[y+1][x-1]+=1
                    if y+1<=14 and x+1<=14 and self.initField[y+1][x+1]!='💣': #vpravo dole
                        self.initField[y+1][x+1]+=1
        self.bombs_num = [] #seznam s poctem min
        for x in self.initField:
            for y in x:
                if y == '💣':
                    self.bombs_num.append(1)
                   
    def createButtons(self): #vytvori pole tlacitek
        self.GroupBox=QGroupBox()
        self.button={}
        horizontalLayout=QHBoxLayout()
        for x in range (15):
            verticalLayout=QVBoxLayout()
            verticalLayout.setSpacing(0)
            for y in range (15):
                self.button[x,y]=QPushButton(self)
                self.button[x,y].setFixedHeight(35)
                self.button[x,y].setFixedWidth(35)
                self.button[x,y].installEventFilter(self)
                self.button[x,y].clicked.connect(partial(self.buttonPressed,x,y))
                self.button[x,y].clicked.connect(self.update_flags)
                self.button[x,y].clicked.connect(self.left_click_start)
                self.button[x,y].is_flagged = False
                self.button[x,y].is_revealed = False
                                  
                verticalLayout.addWidget(self.button[x,y])
                horizontalLayout.addLayout(verticalLayout)
                horizontalLayout.setSpacing(0)
    
        self.GroupBox.setLayout(horizontalLayout)
        
    def buttonPressed(self,x,y):
        a = []
        for i in range(15):
            for j in range(15):
                if self.button[i,j].is_revealed == True:
                    a.append(1)
                if self.button[i,j].is_flagged == True:
                    a.append(1)
                    
        if len(a) == 0:
            if self.new_game == True:
                self.timer.deleteLater()
                self.timer = QTimer()
                self.timer_start_nsecs = int(time.time())
                self.timer.start(1000)
                self.timer.timeout.connect(self.update_timer)
                self.new_game = False
           
        self.showButtons(x,y)
        
        if self.initField[x][y]=='💣':#konec hry
            self.game_over()
            
    def game_over(self):
            self.stop()
            bomb = Bomb(self,'Game over!\nYou hit a bomb.')
            bomb.exec()
            if bomb.clickedButton() == bomb.button_again: #nova hra
                self.timer.deleteLater()
                self.timer = QTimer()
                self.gameField()
                self.opened = []
                self.clock.setText("Time: 000")
                self.new_game = True
                for x in range(15):
                    for y in range(15):
                        self.button[x,y].setText('')
                        self.button[x,y].is_flagged = False
                        self.button[x,y].is_revealed = False  
                        self.button[x,y].clicked.connect(self.left_click_start)
                self.update_flags()
                
            elif bomb.clickedButton() == bomb.button_exit:
              self.close()

    def showButtons(self,x,y): # 0-8:pocet sousednich min, 9: mina, 10:otevrene policko
        if self.initField[x][y]!=0: #kdyz policko ma vedle sebe minu
            if self.initField[x][y]!=10: #a neni otevrene   
                    self.button[x,y].setText(str(self.initField[x][y]))
                    self.button[x,y].is_revealed = True
                    self.button[x,y].is_flagged = False
      
        else: #kdyz policko nema vedle sebe minu
            if self.initField[x][y]!=10: # a neni otevrene 
                   self.button[x,y].setText(str(self.initField[x][y]))#zobrazi nulu
                   self.button[x,y].is_revealed = True
                   self.button[x,y].is_flagged = False
            self.initField[x][y]=10
            if x-1>=0:  #doleva
                self.showButtons(x-1,y)
            if x+1<=14: #doprava
                self.showButtons(x+1,y)
            if y-1>=0: #nahoru
                self.showButtons(x,y-1)
            if y+1<=14: #dolu
                self.showButtons(x,y+1) 
            if x-1>=0 and y-1>=0: #doleva nahoru
                self.showButtons(x-1,y-1)
            if x-1>=0 and y+1<=14: #doleva dolu
                self.showButtons(x-1,y+1)
            if x+1<=14 and y-1>=0:#doprava nahoru
                self.showButtons(x+1,y-1)   
            if x+1<=14 and y+1<=14: #doprava dolu
                self.showButtons(x+1,y+1)
                
    def eventFilter(self, obj, event): #pro prave tlacitko mysi
        if event.type() == QtCore.QEvent.MouseButtonPress:     
            if event.button() == QtCore.Qt.RightButton:
                self.button2 = {}
                for x in range(15):
                    for y in range(15):
                        self.button2[x,y] = self.button[x,y] #dict, (pozice):QPushButton
                self.button3 = {v: k for k, v in self.button2.items()} #otoci dict
                try:
                    self.right_click_start()
                    self.flag(self.button3[obj][0],self.button3[obj][1])
                except KeyError:
                    pass
        return QtCore.QObject.event(obj, event)
    
    def flag(self,x,y):
        self.update_flags()
        
        if self.new_game == True:
            self.timer.timeout.connect(self.update_timer)
            self.new_game = False
            
        if (len(self.bombs_num)) - (len(self.flagsnum)) > 0:
                 if self.button[x,y].is_flagged == False:
                    if self.button[x,y].is_revealed == False:
                        self.button[x,y].setText('🚩')
                        self.button[x,y].is_flagged = True
                 elif self.button[x,y].is_flagged == True:
                    self.button[x,y].setText('')
                    self.button[x,y].is_flagged = False
                    
        elif (len(self.bombs_num)) - (len(self.flagsnum)) == 0: 
             if self.button[x,y].is_flagged == True:
                    self.button[x,y].setText('')
                    self.button[x,y].is_flagged = False
                    
        self.update_flags()
            
        self.flagged = []
        for x in range(15):
            for y in range(15):
                if self.initField[x][y] == '💣' and self.button[x,y].is_flagged == True:
                    self.flagged.append(self.initField[x][y])
        
        if (len(self.bombs_num) == len(self.flagged)): #vyhra
             self.stop() 
             win = Win(self,'Congratulations, you won the game!\nYour time was {} seconds.'.format("%d" % self.n_secs))
             win.exec() 
             if win.clickedButton() == win.button_again:
                self.timer.deleteLater()
                self.timer = QTimer()
                self.opened = [] 
                self.gameField()
                self.clock.setText("Time: 000")
                self.new_game = True
                for x in range(15):
                    for y in range(15):
                        self.button[x,y].setText('')
                        self.button[x,y].is_flagged = False
                        self.button[x,y].is_revealed = False  
                self.update_flags()
                
             elif win.clickedButton() == win.button_exit:
                self.close()
                
    def update_flags(self): #prepisuje zbyvajici vlajky
        self.flagsnum = []
        for x in range(15):
            for y in range(15):
                if self.button[x,y].is_flagged == True:
                    self.flagsnum.append(1)
        self.flags_num.setText('Remaining flags: {}'.format(len(self.bombs_num) - len(self.flagsnum)))
    
    def left_click_start(self):
        self.timer.start(1000)
        for x in range(15):
            for y in range(15):
                self.button[x,y].clicked.disconnect(self.left_click_start)
        self.timer_start_nsecs = int(time.time())
    
    def right_click_start(self):
        self.timer.start(1000)
        for x in range(15):
            for y in range(15): 
                if self.button[x,y].is_flagged == True:
                    self.opened.append(1)
                if self.button[x,y].is_revealed == True:
                    self.opened.append(1)

        if len(self.opened) == 0: #pokud se zacina pravym tlacitkem
            self.timer_start_nsecs = int(time.time())
   
        try:
            for x in range(15):
                for y in range(15): 
                    self.button[x,y].clicked.disconnect(self.left_click_start) 
        except:
            pass
        
    def update_timer(self):
            self.n_secs = int(time.time()) - self.timer_start_nsecs
            self.clock.setText("Time: %03d" % self.n_secs)
  
    def stop(self):
        self.timer.stop()
     
if __name__ == '__main__':
    app = QApplication([])
    window = MainWindow()
    window.show()
    app.exec()
